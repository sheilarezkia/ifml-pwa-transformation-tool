import React from "react";
import './SimpleDateField.css';

const SimpleDateField = props => {
    const { camel_name, dasherized, label, placeholder, defaultValue, ref_func } = props;

    const style = {
    };

    var html_props = {
        style: style,
        name: camel_name,
        id: dasherized,
        placeholder: placeholder ? placeholder : {},
        defaultValue: defaultValue ? defaultValue : "",
        ref: ref_func
    };


    return (
        <div>
            <label className="date_label" htmlFor={dasherized}><strong>{label}</strong></label>
            <div>
                <input
                    className="date_input"
                    type="date"
                    {...html_props}
                />
            </div>
            <br />
        </div>
    );
};

export default SimpleDateField;