import React from "react";
import "./SubMenu.css";

const SubMenu = props => {
    const { variant } = props;
    let nav_class;
    let unordered_list_class;

    switch (variant) {
        case "default":
            nav_class = "submenu_nav_default";
            unordered_list_class = "submenu_list_default";
            break;
        case "orange":
            nav_class = "submenu_nav_orange";
            break;
        case "pink":
            nav_class = "submenu_nav_pink";
            break;
        case "blue":
            nav_class = "submenu_nav_blue";
            unordered_list_class = "submenu_list_blue";
            break;
        default:
            break;
    }

    return (
        <nav className={`submenu_nav ${nav_class}`}>
            <ul className={`submenu_list ${unordered_list_class}`}>
                {props.children}
            </ul>
        </nav>
    );
};

export default SubMenu;