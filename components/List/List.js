import React from "react";
import "./List.css";

const List = props => {
    const { id_name, className, variant, column } = props;

    let list_class;
    let column_class;

    switch (column) {
        case "1":
            column_class = "single_column";
            break;
        case "2":
            column_class = "two_column";
            break;
        case "3":
            column_class = "three_column";
            break;
        default:
            column_class = "single_column";
            break;
    }

    switch (variant) {
        case "default":
            list_class = "ul_default";
            break;
        case "orange":
            list_class = "ul_orange";
            break;
        case "blue":
            list_class = "ul_blue";
            break;
        case "pink":
            list_class = "ul_pink";
            break;
        default:
            break;
    }

    return (
        <ul id={id_name} className={`ul_display ${list_class} ${column_class} ${className}`}>
            {props.children}
        </ul>
    );
};

export default List;