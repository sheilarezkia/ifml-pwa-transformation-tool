import React from "react";
import "./ListItem.css";

const ListItem = props => {
    const { onClick, variant } = props;
    let list_class;

    switch (variant) {
        case "default":
            list_class = "list_default";
            break;
        case "orange":
            list_class = "list_orange";
            break;
        case "blue":
            list_class = "list_blue";
            break;
        case "pink":
            list_class = "list_pink";
            break;
        default:
            list_class = "list_default";
            break;
    }

    const onClickfunc = onClick ? onClick : undefined;

    return (
        <div className={`list_base ${list_class}`} onClick={onClickfunc}>
            {props.children}
        </div>
    );
};

export default ListItem;