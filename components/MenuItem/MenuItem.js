import React from "react";
import "./MenuItem.css";

const MenuItem = props => {
    return (
        <li className={`menu_list`}>
            {props.children}
        </li>
    );
};

export default MenuItem;