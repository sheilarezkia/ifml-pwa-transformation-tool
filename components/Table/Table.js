import React from "react";
import "./Table.css";


const Table = props => {
    const { variant } = props;

    let table_class;

    switch (variant) {
        case "default":
            table_class = "table_default";
            break;
        case "orange":
            table_class = "table_orange";
            break;
        case "blue":
            table_class = "table_blue";
            break;
        case "pink":
            table_class = "table_pink";
            break;
        default:
            break;
    }

    return (
        <table className={`table_base ${table_class}`}>
            {props.children}
        </table>
    );
};

export default Table;