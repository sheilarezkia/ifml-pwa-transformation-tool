import React from "react";
import "./MenuLink.css";

const MenuLink = props => {
    const { href, onClick, variant } = props;

    let menu_link_class;

    switch (variant) {
        case "default":
            menu_link_class = "menu_link_default";
            break;
        case "orange":
            menu_link_class = "menu_link_orange";
            break;
        case "pink":
            menu_link_class = "menu_link_pink";
            break;
        case "blue":
            menu_link_class = "menu_link_blue";
            break;
        default:
            menu_link_class = "menu_link_default";
            break;
    }

    return (
        <a href={href} onClick={onClick} className={`menu_link ${menu_link_class}`}>
            {props.children}
        </a>
    );
};

export default MenuLink;