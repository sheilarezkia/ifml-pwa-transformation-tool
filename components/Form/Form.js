import React from "react";
import "./Form.css";

const Form = props => {
    const { title, on_submit, id_name, variant } = props;

    let fieldset_class;
    let heading_class;

    switch (variant) {
        case "default":
            fieldset_class = "fieldset_default";
            heading_class = "heading_default";
            break;
        case "orange":
            fieldset_class = "fieldset_orange";
            heading_class = "heading_orange";
            break;
        case "pink":
            fieldset_class = "fieldset_pink";
            heading_class = "heading_pink";
            break;
        case "blue":
            fieldset_class = "fieldset_blue";
            heading_class = "heading_blue";
            break;
        default:
            fieldset_class = "fieldset_default";
            heading_class = "heading_default";
            break;
    }

    return (
        <form className="basic_form" id={id_name} {...on_submit}>
            <fieldset className={`form_fieldset ${fieldset_class}`}>
                <h2 className={`form_heading ${heading_class}`}>{title}</h2>
                {props.children}
            </fieldset>
        </form>
    );
};

export default Form;