import React from "react";
import "./InputField.css";

const InputField = props => {
    const { camel_name, type, dasherized, label, placeholder, defaultValue, ref_func, variant } = props;
    let label_class;
    let input_class;

    switch (variant) {
        case "default":
            input_class = "input_default";
            label_class = "label_default";
            break;
        case "orange":
            input_class = "input_orange";
            label_class = "label_orange";
            break;
        case "blue":
            input_class = "input_blue";
            label_class = "label_blue";
            break;
        case "pink":
            input_class = "input_pink";
            label_class = "label_pink";
            break;
        default:
            input_class = "input_default";
            label_class = "label_default";
            break;
    }

    var html_props = {
        className: `input_base ${input_class}`,
        name: camel_name,
        id: dasherized,
        type: type,
        placeholder: placeholder ? placeholder : {},
        defaultValue: defaultValue ? defaultValue : "",
        ref: ref_func
    };

    return (
        <div>
            {{ label } ? <label className={`input_label ${label_class}`} htmlFor={dasherized}><strong>{label}</strong></label> : null}
            <div>
                <input
                    {...html_props}
                />
            </div>
            <br />
        </div>
    );
};

export default InputField;