import React from "react";
import "./TableRow.css";


const TableRow = props => {
    const { variant, onClick } = props;

    let table_row_class;

    switch (variant) {
        case "default":
            table_row_class = "tr_default";
            break;
        case "orange":
            table_row_class = "tr_orange";
            break;
        case "blue":
            table_row_class = "tr_blue";
            break;
        case "pink":
            table_row_class = "tr_pink";
            break;
        default:
            break;
    }

    const onClickfunc = onClick ? onClick : undefined;

    return (
        <tr className={`tr_base ${table_row_class}`} onClick={onClickfunc}>
            {props.children}
        </tr>
    );
};

export default TableRow;