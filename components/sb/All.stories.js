import React from 'react';
import {
    Button,
    Detail,
    Form,
    InputField,
    VisualizationAttr,
    ListItem,
    Table,
    TableBody,
    TableHead,
    TableRow, TableCell
} from '..';
// , ListItem, InputField, Form, Table, TableCell, TableRow, TableHead, TableBody 
export default {
    title: 'All',
    component: { Button, Detail, InputField, Form },
};

export const button = () => (
    <div>
        <Button variant="blue" text="Hello" />
        <Button variant="orange" text="Hello" />
        <Button variant="pink" text="Hello" />
        <Button variant="default" text="Hello" />
    </div>
);
export const input = () => (
    <div>
        <InputField variant="blue" label="Hello" camel_name="test" defaultValue="Hello" />
        <InputField variant="orange" label="Hello" camel_name="test" defaultValue="Hello" />
        <InputField variant="pink" label="Hello" camel_name="test" defaultValue="Hello" />
        <InputField variant="default" label="Hello" camel_name="test" defaultValue="Hello" />
    </div>
);
export const form = () => (
    <div>
        <Form title="Hello" id_name="test_form" variant="blue">
            <InputField variant="blue" label="Hello" camel_name="test" defaultValue="Hello" />
        </Form>
        <Form title="Hello" id_name="test_form" variant="orange">
            <InputField variant="orange" label="Hello" camel_name="test" defaultValue="Hello" />
        </Form>
        <Form title="Hello" id_name="test_form" variant="pink">
            <InputField variant="pink" label="Hello" camel_name="test" defaultValue="Hello" />
        </Form>
        <Form title="Hello" id_name="test_form" variant="default">
            <InputField variant="default" label="Hello" camel_name="test" defaultValue="Hello" />
        </Form>
    </div>
);

export const detail = () => (
    <div>
        <Detail variant="blue">
            <VisualizationAttr title_name="Test Title" content="value" variant="blue" />
        </Detail>
        <Detail variant="orange">
            <VisualizationAttr title_name="Test Title" content="value" variant="orange" />
        </Detail>
        <Detail variant="pink">
            <VisualizationAttr title_name="Test Title" content="value" variant="pink" />
        </Detail>
        <Detail variant="default">
            <VisualizationAttr title_name="Test Title" content="value" variant="default" />
        </Detail>
    </div>
);

export const visualization_attribute = () => (
    <div>
        <VisualizationAttr title_name="Test Title" content="value" variant="blue" />
        <VisualizationAttr title_name="Test Title" content="value" variant="orange" />
        <VisualizationAttr title_name="Test Title" content="value" variant="pink" />
        <VisualizationAttr title_name="Test Title" content="value" variant="default" />
    </div>
);

export const list_item = () => (
    <div>
        <ListItem variant="blue">
            <VisualizationAttr title_name="Test Title" content="value" variant="blue" />
        </ListItem>
        <ListItem variant="orange">
            <VisualizationAttr title_name="Test Title" content="value" variant="orange" />
        </ListItem>
        <ListItem variant="pink">
            <VisualizationAttr title_name="Test Title" content="value" variant="pink" />
        </ListItem>
        <ListItem variant="default">
            <VisualizationAttr title_name="Test Title" content="value" variant="default" />
        </ListItem>
    </div>
);

export const table_head = () => (
    <div>
        <Table variant="blue">
            <TableHead>
                <TableRow variant='blue'>

                    <TableCell id='cell1' variant='blue'>Cell 1</TableCell>

                    <TableCell id='cell2' variant='blue'>Cell 2</TableCell>

                    <TableCell id='cell3' variant='blue'>Cell 3</TableCell>

                </TableRow>
            </TableHead>
        </Table>

        <Table variant="orange">
            <TableHead>
                <TableRow variant='orange'>

                    <TableCell id='cell1' variant='orange'>Cell 1</TableCell>

                    <TableCell id='cell2' variant='orange'>Cell 2</TableCell>

                    <TableCell id='cell3' variant='orange'>Cell 3</TableCell>

                </TableRow>
            </TableHead>
        </Table>

        <Table variant="pink">
            <TableHead>
                <TableRow variant='pink'>

                    <TableCell id='cell1' variant='pink'>Cell 1</TableCell>

                    <TableCell id='cell2' variant='pink'>Cell 2</TableCell>

                    <TableCell id='cell3' variant='pink'>Cell 3</TableCell>

                </TableRow>
            </TableHead>
        </Table>

        <Table variant="default">
            <TableHead>
                <TableRow variant='default'>

                    <TableCell id='cell1' variant='default'>Cell 1</TableCell>

                    <TableCell id='cell2' variant='default'>Cell 2</TableCell>

                    <TableCell id='cell3' variant='default'>Cell 3</TableCell>

                </TableRow>
            </TableHead>
        </Table>
    </div>
);

export const table_body = () => (
    <div>
        <TableBody>
            <TableRow variant='blue'>

                <TableCell id='cell1' variant='blue'>Cell 1</TableCell>

                <TableCell id='cell2' variant='blue'><InputField variant="blue" camel_name="test" defaultValue="Hello" /></TableCell>

                <TableCell id='cell3' variant='blue'><Button variant="blue" text="Hello" /></TableCell>

            </TableRow>
        </TableBody>
        <br></br>
        <TableBody>
            <TableRow variant='orange'>

                <TableCell id='cell1' variant='orange'>Cell 1</TableCell>

                <TableCell id='cell2' variant='orange'><InputField variant="orange" camel_name="test" defaultValue="Hello" /></TableCell>

                <TableCell id='cell3' variant='orange'><Button variant="orange" text="Hello" /></TableCell>

            </TableRow>
        </TableBody>
        <br></br>
        <TableBody>
            <TableRow variant='pink'>

                <TableCell id='cell1' variant='pink'>Cell 1</TableCell>

                <TableCell id='cell2' variant='pink'><InputField variant="pink" camel_name="test" defaultValue="Hello" /></TableCell>

                <TableCell id='cell3' variant='pink'><Button variant="pink" text="Hello" /></TableCell>

            </TableRow>
        </TableBody>
        <br></br>
        <TableBody>
            <TableRow variant='default'>

                <TableCell id='cell1' variant='default'>Cell 1</TableCell>

                <TableCell id='cell2' variant='default'><InputField variant="default" camel_name="test" defaultValue="Hello" /></TableCell>

                <TableCell id='cell3' variant='default'><Button variant="default" text="Hello" /></TableCell>

            </TableRow>
        </TableBody>
    </div>
);