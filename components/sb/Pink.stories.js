import React from 'react';
import {
    Button,
    Detail,
    Form,
    InputField,
    VisualizationAttr,
    ListItem,
    List,
    Table,
    TableBody,
    TableHead,
    TableRow, TableCell
} from '..';
export default {
    title: 'Pink',
    component: { Button, Detail, InputField, Form },
};

export const button = () => (
    <Button variant="pink" text="Hello" />
);
export const input = () => <InputField variant="pink" label="Hello" camel_name="test" defaultValue="Hello" />
export const form = () => (
    <Form title="Hello" id_name="test_form" variant="pink">
        <InputField variant="pink" label="Hello" camel_name="test" defaultValue="Hello" />
    </Form>
);

export const detail = () => (
    <Detail variant="pink">
        <VisualizationAttr title_name="Test Title" content="value" variant="pink" />
    </Detail>
);

export const visualization_attribute = () => (
    <VisualizationAttr title_name="Test Title" content="value" variant="pink" />
);

export const list_item = () => (
    <List id_name="test_list" className="test" variant="pink" column="1">
        <ListItem variant="pink">
            <VisualizationAttr title_name="Test Title" content="value" variant="pink" />
            <InputField variant="pink" label="Hello" camel_name="test" defaultValue="Hello" />
            <Button variant="pink" text="Hello" />
        </ListItem>
    </List>
);

export const table_head = () => (
    <Table variant="pink">
        <TableHead>
            <TableRow variant='pink'>
                <TableCell id='cell1' variant='pink'>Cell 1</TableCell>
                <TableCell id='cell2' variant='pink'>Cell 2</TableCell>
                <TableCell id='cell3' variant='pink'>Cell 3</TableCell>
            </TableRow>
        </TableHead>
    </Table>
);

export const table_body = () => (
    <TableBody>
        <TableRow variant='pink'>
            <TableCell id='cell1' variant='pink'>Cell 1</TableCell>
            <TableCell id='cell2' variant='pink'><InputField variant="pink" camel_name="test" defaultValue="Hello" /></TableCell>
            <TableCell id='cell3' variant='pink'><Button variant="pink" text="Hello" /></TableCell>
        </TableRow>
    </TableBody>
);