import React from 'react';
import {
    Button,
    Detail,
    Form,
    InputField,
    VisualizationAttr,
    ListItem,
    List,
    Table,
    TableBody,
    TableHead,
    TableRow, TableCell
} from '..';
// , ListItem, InputField, Form, Table, TableCell, TableRow, TableHead, TableBody 
export default {
    title: 'Blue',
    component: { Button, Detail, InputField, Form },
};

export const button = () => (
    <Button variant="blue" text="Hello" />
);
export const input = () => <InputField variant="blue" label="Hello" camel_name="test" defaultValue="Hello" />
export const form = () => (
    <Form title="Hello" id_name="test_form" variant="blue">
        <InputField variant="blue" label="Hello" camel_name="test" defaultValue="Hello" />
    </Form>
);

export const detail = () => (
    <Detail variant="blue">
        <VisualizationAttr title_name="Test Title" content="value" variant="blue" />
    </Detail>
);

export const visualization_attribute = () => (
    <VisualizationAttr title_name="Test Title" content="value" variant="blue" />
);

export const list_item = () => (
    <List id_name="test_list" className="test" variant="blue" column="1">
        <ListItem variant="blue">
            <VisualizationAttr title_name="Test Title" content="value" variant="blue" />
            <InputField variant="blue" label="Hello" camel_name="test" defaultValue="Hello" />
            <Button variant="blue" text="Hello" />
        </ListItem>
    </List>
);

export const table_head = () => (
    <Table variant="blue">
        <TableHead>
            <TableRow variant='blue'>

                <TableCell id='cell1' variant='blue'>Cell 1</TableCell>

                <TableCell id='cell2' variant='blue'>Cell 2</TableCell>

                <TableCell id='cell3' variant='blue'>Cell 3</TableCell>

            </TableRow>
        </TableHead>
    </Table>
);

export const table_body = () => (
    <TableBody>
        <TableRow variant='blue'>
            <TableCell id='cell1' variant='blue'>Cell 1</TableCell>
            <TableCell id='cell2' variant='blue'><InputField variant="blue" camel_name="test" defaultValue="Hello" /></TableCell>
            <TableCell id='cell3' variant='blue'><Button variant="blue" text="Hello" /></TableCell>
        </TableRow>
    </TableBody>
);