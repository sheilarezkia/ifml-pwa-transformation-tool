import React from 'react';
import {
    Button,
    Detail,
    Form,
    InputField,
    VisualizationAttr,
    ListItem,
    List,
    Table,
    TableBody,
    TableHead,
    TableRow, TableCell
} from '..';
export default {
    title: 'Default',
    component: { Button, Detail, InputField, Form },
};

export const button = () => (
    <Button variant="default" text="Hello" />
);
export const input = () => <InputField variant="default" label="Hello" camel_name="test" defaultValue="Hello" />
export const form = () => (
    <Form title="Hello" id_name="test_form" variant="default">
        <InputField variant="default" label="Hello" camel_name="test" defaultValue="Hello" />
    </Form>
);

export const detail = () => (
    <Detail variant="default">
        <VisualizationAttr title_name="Test Title" content="value" variant="default" />
    </Detail>
);

export const visualization_attribute = () => (
    <VisualizationAttr title_name="Test Title" content="value" variant="default" />
);

export const list_item = () => (
    <List id_name="test_list" className="test" variant="default" column="1">
        <ListItem variant="default">
            <VisualizationAttr title_name="Test Title" content="value" variant="default" />
            <InputField variant="default" label="Hello" camel_name="test" defaultValue="Hello" />
            <Button variant="default" text="Hello" />
        </ListItem>
    </List>
);

export const table_head = () => (
    <Table variant="default">
        <TableHead>
            <TableRow variant='default'>
                <TableCell id='cell1' variant='default'>Cell 1</TableCell>
                <TableCell id='cell2' variant='default'>Cell 2</TableCell>
                <TableCell id='cell3' variant='default'>Cell 3</TableCell>
            </TableRow>
        </TableHead>
    </Table>
);

export const table_body = () => (
    <TableBody>
        <TableRow variant='default'>
            <TableCell id='cell1' variant='default'>Cell 1</TableCell>
            <TableCell id='cell2' variant='default'><InputField variant="default" camel_name="test" defaultValue="Hello" /></TableCell>
            <TableCell id='cell3' variant='default'><Button variant="default" text="Hello" /></TableCell>
        </TableRow>
    </TableBody>
);