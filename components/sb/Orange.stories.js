import React from 'react';
import {
    Button,
    Detail,
    Form,
    InputField,
    VisualizationAttr,
    ListItem,
    List,
    Table,
    TableBody,
    TableHead,
    TableRow, TableCell
} from '..';
// , ListItem, InputField, Form, Table, TableCell, TableRow, TableHead, TableBody 
export default {
    title: 'Orange',
    component: { Button, Detail, InputField, Form },
};

export const button = () => (
    <Button variant="orange" text="Hello" />
);
export const input = () => <InputField variant="orange" label="Hello" camel_name="test" defaultValue="Hello" />
export const form = () => (
    <Form title="Hello" id_name="test_form" variant="orange">
        <InputField variant="orange" label="Hello" camel_name="test" defaultValue="Hello" />
    </Form>
);

export const detail = () => (
    <Detail variant="orange">
        <VisualizationAttr title_name="Test Title" content="value" variant="orange" />
    </Detail>
);

export const visualization_attribute = () => (
    <VisualizationAttr title_name="Test Title" content="value" variant="orange" />
);

export const list_item = () => (
    <List id_name="test_list" className="test" variant="orange" column="1">
        <ListItem variant="orange">
            <VisualizationAttr title_name="Test Title" content="value" variant="orange" />
            <InputField variant="orange" label="Hello" camel_name="test" defaultValue="Hello" />
            <Button variant="orange" text="Hello" />
        </ListItem>
    </List>
);

export const table_head = () => (
    <Table variant="orange">
        <TableHead>
            <TableRow variant='orange'>
                <TableCell id='cell1' variant='orange'>Cell 1</TableCell>
                <TableCell id='cell2' variant='orange'>Cell 2</TableCell>
                <TableCell id='cell3' variant='orange'>Cell 3</TableCell>
            </TableRow>
        </TableHead>
    </Table>
);

export const table_body = () => (
    <TableBody>
        <TableRow variant='orange'>
            <TableCell id='cell1' variant='orange'>Cell 1</TableCell>
            <TableCell id='cell2' variant='orange'><InputField variant="orange" camel_name="test" defaultValue="Hello" /></TableCell>
            <TableCell id='cell3' variant='orange'><Button variant="orange" text="Hello" /></TableCell>
        </TableRow>
    </TableBody>
);