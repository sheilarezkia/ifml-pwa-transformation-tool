import React from "react";


const TableHead = props => {
    return (
        <thead className={`thead_base`}>
            {props.children}
        </thead>
    );
};

export default TableHead;