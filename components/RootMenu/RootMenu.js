import React from "react";
import "./RootMenu.css";

const RootMenu = props => {

    const { variant } = props;

    let nav_root_class;
    let root_span_class;

    switch (variant) {
        case "default":
            nav_root_class = "root_default";
            root_span_class = "root_span_default";
            break;
        case "orange":
            nav_root_class = "root_orange";
            break;
        case "pink":
            nav_root_class = "root_pink";
            break;
        case "blue":
            nav_root_class = "root_blue";
            root_span_class = "root_span_blue";
            break;
        default:
            nav_root_class = "root_default";
            root_span_class = "root_span_default";
            break;
    }

    const renderChildren = () => {
        if (props.isAuth) {
            return (
                <nav className={`nav_root ${nav_root_class}`}>
                    <span className={`${root_span_class}`}>
                        <React.Fragment>
                            {props.children}
                        </React.Fragment>
                    </span>
                </nav>
            );
        }
        return null;
    }

    return renderChildren();
};

export default RootMenu;