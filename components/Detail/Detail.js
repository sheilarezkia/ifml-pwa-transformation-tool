import React from "react";
import "./Detail.css";

const Detail = props => {
    const { variant } = props;

    let detail_class;

    switch (variant) {
        case "default":
            detail_class = "detail_default";
            break;
        case "orange":
            detail_class = "detail_orange";
            break;
        case "blue":
            detail_class = "detail_blue";
            break;
        case "pink":
            detail_class = "detail_pink";
            break;
        default:
            detail_class = "detail_default";
            break;
    }

    return (
        <div className={`detail_div ${detail_class}`}>
            {props.children}
        </div>
    );
};

export default Detail;