import React from "react";
import "./VisualizationAttr.css";


const VisualizationAttr = props => {
    const { title_name, content, variant } = props;

    let label_class;

    switch (variant) {
        case "default":
            label_class = "visual_label_default";
            break;
        case "orange":
            label_class = "visual_label_orange";
            break;
        case "blue":
            label_class = "visual_label_blue";
            break;
        case "pink":
            label_class = "visual_label_pink";
            break;
        default:
            break;
    }

    return (
        <div className="visualization_div">
            <br />
            <label className={`visualization_label ${label_class}`}>{title_name}</label>
            <div className={`visualization_content`}>{content}</div>
            <br />
        </div>
    );
};

export default VisualizationAttr;