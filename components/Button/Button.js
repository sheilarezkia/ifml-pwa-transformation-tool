import React from "react";
import "./Button.css";

const Button = props => {
    const { variant, disabled, text, onClick } = props;

    let button_class;

    switch (variant) {
        case "default":
            button_class = "button_default";
            break;
        case "orange":
            button_class = "button_orange";
            break;
        case "blue":
            button_class = "button_blue";
            break;
        case "pink":
            button_class = "button_pink";
            break;
        default:
            button_class = "button_default";
            break;
    }

    return (
        <div className="btn_block">
            <button disabled={disabled} className={`btn ${button_class}`} onClick={onClick} >
                {text}
            </button>
        </div>
    );
};

export default Button;