import React from "react";
import "./TableCell.css";


const TableCell = props => {
    const { variant } = props;

    let table_cell_class;

    switch (variant) {
        case "default":
            table_cell_class = "td_default";
            break;
        case "orange":
            table_cell_class = "td_orange";
            break;
        case "blue":
            table_cell_class = "td_blue";
            break;
        case "pink":
            table_cell_class = "td_pink";
            break;
        default:
            break;
    }

    return (
        <td className={`td_base ${table_cell_class}`}>
            {props.children}
        </td>
    );
};

export default TableCell;