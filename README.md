# How To: IFML Transformation Tool - React

## Generating React Application
Sebelum membuat aplikasi dengan *generator*, pastikan terlebih dahulu semua *dependency* Python milik *generator* sudah terinstall dengan baik. Dibutuhkan Python dengan versi `3.7` untuk menjalankan *generator*.

Setelah menggunakan versi Python yang tepat, *install* semua *dependency* dengan menggunakan `pip`.

    pip install -r requirements.txt
  
Apabila seluruh *dependency* sudah ter-*installl*, jalankan perintah berikut ini pada *root directory*:

    python __init__.py app_config_req.json react
Berkas `app_config_req.json` adalah berkas yang berisikan pengaturan untuk penggunaan Komponen UI pada aplikasi React yang digunakan, penjelasan untuk pengaturan berkas ini dapat [dilihat pada Configuration Settings](#configuration-settings).

Setelah perintah di atas selesai dieksekusi, pada *root directory* akan terdapat *directory* bernama `result`. Dalam *directory* `result`, terdapat *directory* yang merupakan aplikasi React yang dihasilkan *generator*, yaitu `financial-sample`.

## Building the App
Apabila ingin dilakukan *build* aplikasi, maka pastikan Anda berada pada *directory* `result/financial-sample`. Setelah berada pada *directory* tersebut, jalankan perintah `npm install`.

Perintah `npm install` akan melakukan instalasi seluruh *dependency* yang dibutuhkan, termasuk Storybook. Selanjutnya, [aplikasi dapat dijalankan](#running-the-app).

## Running the App
Terdapat beberapa langkah yang perlu dilakukan sebelum menjalankan aplikasi. 

#### ABS Microservices Back-end
Pertama, pastikan terlebih dahulu bahwa `abs-microservices` sudah berjalan pada mesin yang Anda gunakan. Lihat cara menjalankan `abs-microservices` pada [*repository* ABS Microservices](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/abs-microservices/-/tree/affan/dev). 
**Important Note**: Modifikasi terhadap autentikasi yang digunakan pada  Route milik ABS Microservices mungkin perlu dilakukan terlebih dahulu.

#### Static Page Back-end
Selanjutnya, pastikan bahwa `static-page-backend` sudah berjalan dengan baik. `static-page-backend` dapat dilihat pada [*repository* Static Page](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/static-page-backend). Setelah *clone repository* tersebut, pastikan juga Anda menggunakan Python versi `3.7`. Kemudian, *install* seluruh *dependency* yang dibutuhkan `static-page-backend` dengan menjalankan perintah:

    pip install -r requirements.txt
 Kemudian, `static-page-backend` dapat dijalankan dengan perintah:

    python app.py
  
#### Running React Application
  Aplikasi React yang dihasilkan oleh *generator* dapat dijalankan dengan melakukan perintah `npm start`. Aplikasi dapat diakses pada `http://localhost:3000/`.
  
#### Running Storybook
Komponen UI yang digunakan pada aplikasi dapat dilihat pada Storybook, untuk menjalankan Storybook perlu dijalankan perintah `npm run storybook`. Storybook dapat diakses pada `http://localhost:9009/`.

## Important Directories
Komponen UI yang dibuat untuk aplikasi dapat dilihat pada *directory* `components` yang berada dalam *root directory*. Apabila komponen UI yang ingin digunakan pada aplikasi yang dihasilkan *generator* ingin ditambah atau diubah, maka lakukan perubahan pada isi *directory* ini.

Untuk menambahkan komponen agar dapat dilihat pada Storybook, Anda perlu melakukan modifikasi pada berkas-berkas yang terletak pada *directory* `components/sb`.

## Configuration Settings

Konfigurasi perlu dilakukan pada berkas `app_config_req.json` untuk penggunaan komponen pada aplikasi. `app_config_req.json` dapat memiliki sebuah *key* `interface_variation`. *Key* `interface_variation` merupakan sebuah *dictionary* yang menerima beberapa *key* yang akan mengatur penggunaan komponen dan variasi tampilan pada aplikasi.

Tabel di bawah ini menunjukkan *key* yang dapat menyusun *dictionary* `interface_variation`.

| *Key* |*Value Type*  | Keterangan |
|--|--|--|
| `layouts` | *dictionary* | *dictionary* ini dapat memiliki beberapa key, yaitu `list_column_count` dan `feature_list_display`. `list_column_count` akan menentukan jumlah kolom pada *unordered list*. Sementara pada `feature_list_display` dapat dispesifikasikan apakah *list of items* pada sebuah fitur akan ditampilkan dengan `"table"`, atau dengan `"list"`. |
| `themes`      |*dictionary*|*dictionary* ini dapat memiliki beberapa key, yaitu `theme`, `button`, `simple_field`, `list`, `form`, `detail` dan `visualization_span`. Masing-masing *key* yang telah disebutkan, dapat memiliki nilai yaitu *string* `"default"`, `"orange"`, `"blue"`, atau `"pink"`. |
| `component_orders`          |*dictionary*| *Key* dalam *dictionary* ini adalah `id` dari `Form`, `List`, atau `Detail` yang akan diacak ulang urutan elemennya. *Value* dari tiap *key* adalah sebuah *array*. *Array* tersebut berisikan *string* of `id`s dari setiap elemen penyusun `Form`, `List`, atau `Detail` yang dituliskan pada *key* tersebut. Urutan `id` pada *array* menunjukkan urutan elemen yang diinginkan pada `Form`, `List`, atau `Detail` yang dituliskan sebagai *key*. |
