from pathlib import Path
from settings import setting


def find_path(file_name, search_path='/', type=None):
    file_path = get_search_path(file_name) if type is None else type
    return Path(search_path + '/' + file_path)


def get_search_path(file_name, suffix=None):
    suff = file_name.split('.')[1].lower() if suffix is None else suffix
    if suff == 'uml':
        return 'uml'
    elif suff == 'core':
        return 'ifml'
    elif suff == 'json':
        return 'json'


def parse_input(file_name, search_path=setting.DEFAULT_SEARCH_PATH, type=None):
    return open(str(find_path(file_name, search_path, type) / file_name), 'r')


def is_file_exists(file_name, search_path=setting.DEFAULT_SEARCH_PATH):
    try:
        parse_input(file_name, search_path)
        return True
    except FileNotFoundError:
        return False
