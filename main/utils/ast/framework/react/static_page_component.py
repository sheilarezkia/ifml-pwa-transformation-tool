from main.utils.ast.framework.react.components import \
    ReactComponentEseightClass, ReactJSX
from main.utils.ast.framework.react.routers import RouteToAction
from main.utils.jinja.react import base_file_writer, react_jsx_writer


class HomepageJSX(ReactJSX):
    def __init__(self, interface_theme):
        self.interface_theme = interface_theme
        super().__init__()

    def render(self):
        return base_file_writer(
            'src/Homepage/homepage.jsx.template',
            interface_theme=self.interface_theme
        )

class LandingPageJSX(ReactJSX):
    def __init__(self, interface_theme):
        self.interface_theme = interface_theme
        super().__init__()
    
    def render(self):
        return base_file_writer(
            'src/Homepage/landing_page.jsx.template',
            interface_theme=self.interface_theme
        )

class AboutUsJSX(ReactJSX):
    def __init__(self, interface_theme):
        self.interface_theme = interface_theme
        super().__init__()

    def render(self):
        return base_file_writer(
            'src/Homepage/about_us.jsx.template',
            interface_theme=self.interface_theme
        )

class ContactUsJSX(ReactJSX):

    def __init__(self, interface_theme):
        super().__init__()
        self.interface_theme = interface_theme

    def render(self):
        return base_file_writer(
            'src/Homepage/contact_us.jsx.template', 
            interface_theme=self.interface_theme
        )

class LandingPageRoute(RouteToAction):

    def __init__(self, service_class_name, service_filename):
        super().__init__(service_class_name, service_filename)
    
    def render(self):
        return base_file_writer(
            'src/Homepage/landing_page_route.template'
        )

class AboutUsRoute(RouteToAction):

    def __init__(self, service_class_name, service_filename):
        super().__init__(service_class_name, service_filename)
    
    def render(self):
        return base_file_writer(
            'src/Homepage/about_us_route.template'
        )

class ContactUsRoute(RouteToAction):

    def __init__(self, service_class_name, service_filename):
        super().__init__(service_class_name, service_filename)
    
    def render(self):
        return base_file_writer(
            'src/Homepage/contact_us_route.template'
        )