module.exports = {
    stories: ['../src/components/sb/*.stories.js'],
    addons: [
        '@storybook/preset-create-react-app',
        '@storybook/addon-actions',
        '@storybook/addon-links',
        '@storybook/addon-storysource',
    ],
};